PostGIS
-------

Mini version of PostgreSQL with PostGIS extension.

Includes all `extension` contribs from original source code, and PostGIS extension.

Run
---

    docker run -d --name postgis --restart unless-stopped -it -p 5432:5432 lontongcorp/postgis


Connect
-------

From host or remotely:

    psql -U postgres -d template1 -W


Test
----

Create new postgis-enabled database

    psql> CREATE DATABASE mydb;
    psql> \c mydb
    psql> CREATE EXTENSION hstore;
    psql> CREATE EXTENSION dblink;
    psql> CREATE EXTENSION postgis;
    psql> CREATE EXTENSION postgis_topology;
    psql> CREATE EXTENSION postgis_tiger_geocoder;