FROM alpine:3.4

ENV LANG en_US.utf8
ENV PGDATA /var/lib/postgresql
ENV PGPASS 'mySecretP4ssword'

ENV PACKAGES="curl make gcc g++ linux-headers libgcc"
ENV GEOS_DEV="git automake autoconf libtool"

RUN apk add --no-cache ${PACKAGES} libxml2 json-c libstdc++ && \
    mkdir ${PGDATA} && chown -R postgres:postgres ${PGDATA} && cd /tmp && \
    curl -o postgresql-9.5.4.tar.gz -sSL https://ftp.postgresql.org/pub/source/v9.5.4/postgresql-9.5.4.tar.gz && \
    curl -o postgresql-9.5.4.tar.gz.sha256 -sSL https://ftp.postgresql.org/pub/source/v9.5.4/postgresql-9.5.4.tar.gz.sha256 && \
    sha256sum -c postgresql-9.5.4.tar.gz.sha256 && \
    tar -zxf postgresql-9.5.4.tar.gz && \
    cd postgresql-9.5.4 && \
    ./configure --prefix=/usr --disable-debug --without-zlib --without-readline && \
    make && make install-strip && \
    cd contrib && make && make install-strip && \
    su - postgres -c "/usr/bin/initdb -D ${PGDATA}" >> /dev/null $2>1 && \
    su - postgres -c "/usr/bin/pg_ctl -D ${PGDATA} start" >> /dev/null $2>1 && \
    sleep 1s && \
    psql -U postgres -d template1 -c "UPDATE pg_database SET datistemplate='false' WHERE datname IN ('template0', 'postgres');" >> /dev/null $2>1 && \
    psql -U postgres -d template1 -c "DROP DATABASE template0;" >> /dev/null $2>1 && \
    psql -U postgres -d template1 -c "DROP DATABASE postgres;" >> /dev/null $2>1 && \
    psql -U postgres -d template1 -c "ALTER ROLE postgres WITH PASSWORD '${PGPASS}'" >> /dev/null $2>1 && \
    su - postgres -c "/usr/bin/pg_ctl -D ${PGDATA} stop" >> /dev/null $2>1 && \
    echo "local  all  all            md5" > ${PGDATA}/pg_hba.conf && \
    echo "host   all  all  0.0.0.0/0 md5" >> ${PGDATA}/pg_hba.conf && \
    echo "listen_addresses='*'" >> ${PGDATA}/postgresql.conf && \
    cd /tmp && \
    curl -o proj-4.9.1.tar.gz -sSL http://download.osgeo.org/proj/proj-4.9.1.tar.gz && \
    tar -zxf proj-4.9.1.tar.gz && cd proj-4.9.1 && \
    ./configure --prefix=/usr --enable-static && \
    make && make install && \
    cd ../ && \
    apk add --no-cache ${GEOS_DEV} && \
    git clone https://github.com/libgeos/libgeos && \
    cd libgeos && git checkout svn-3.5 && sh autogen.sh && \
    ./configure --prefix=/usr --enable-static && \
    make && make install && \
    cd ../ && \
    apk del ${GEOS_DEV} && \
    curl -o gdal-2.1.1.tar.xz -sSL http://download.osgeo.org/gdal/2.1.1/gdal-2.1.1.tar.xz && \
    tar -xf gdal-2.1.1.tar.xz && cd gdal-2.1.1 && \
    ./configure --prefix=/usr --without-grib --without-mrf --without-pam --without-libtool && \
    make  && make install && \
    cd ../ && \
    apk add --no-cache perl libxml2-dev json-c-dev && \
    curl -o postgis-2.2.2.tar.gz -sSL http://download.osgeo.org/postgis/source/postgis-2.2.2.tar.gz && \
    tar -zxf postgis-2.2.2.tar.gz && cd postgis-2.2.2 && ls /usr/bin && \
    LDFLAGS=-lstdc++ ./configure --prefix=/usr --enable-static --with-geosconfig=/usr/bin/geos-config && \
    make  && make install && \
    apk del ${PACKAGES} perl libxml2-dev json-c-dev && \
    mv /usr/bin/pg_ctl /usr/local/bin/ && cd /usr/bin && \
    rm -rf /usr/share/doc /usr/share/man /usr/share/gdal /usr/share/postgresql/*.sample \
            /etc/ssl /usr/include /var/cache/apk/* /tmp/* /usr/lib/postgresql/pgxs initdb \
            /usr/share/postgresql/contrib clusterdb createdb createlang createuser dropdb \
            droplang dropuser ecpg reindexdb vacuumdb vacuumlo oid2name testepsg nearblack \
            pg* cs2cs nad2bin gdal* ogr* *proj* geos* *2pgsql *geod* && \
    mv /usr/local/bin/pg_ctl /usr/bin/


EXPOSE 5432

USER postgres

CMD ["/usr/bin/postgres"]
